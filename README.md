# apache2

Apache HTTP Server ("httpd"). https://httpd.apache.org

## Official documentation
* [*Apache Tutorial: Dynamic Content with CGI*
  ](https://httpd.apache.org/docs/2.4/en/howto/cgi.html)

### Modules
* [Module Index
  ](https://httpd.apache.org/docs/2.4/en/mod/)
* [*mod_cgi*
  ](https://httpd.apache.org/docs/2.4/en/mod/mod_cgi.html)
* [*mod_cgid*
  ](https://httpd.apache.org/docs/2.4/en/mod/mod_cgid.html)
* [*mod_fcgid*
  ](https://httpd.apache.org/mod_fcgid/)
* [*mod_proxy_wstunnel*
  ](http://httpd.apache.org/docs/2.4/mod/mod_proxy_wstunnel.html)

## Unofficial documentation
* [apache debian tutorial](https://google.com/search?q=apache+debian+tutorial)
* [*Web Server (HTTP)*
  ](https://www.debian.org/doc/manuals/debian-handbook/sect.http-web-server.en.html)
  The Debian Administrator's Handbook
  * https://debian-handbook.info/browse/stable/sect.http-web-server.html
* [*How To Install the Apache Web Server on Debian 9*
  ](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-debian-9)
  2018-09 Justin Ellingwood, Kathleen Juell and Hanif Jetha
* [*How To Use Apache HTTP Server As Reverse-Proxy Using mod_proxy Extension*
  ](https://www.digitalocean.com/community/tutorials/how-to-use-apache-http-server-as-reverse-proxy-using-mod_proxy-extension)
  2014-02-14 O.S. Tezer

### CGI
* [*CGI Tutorial*](http://cgi.tutorial.codepoint.net/) 2006-2015 Clodoaldo Pinto Neto